const botSettings = require("./botsettings.json");
const Discord = require("discord.js");
const fs = require ("fs");

/*
COPYRIGHT © Foxbit#6845 - @itsfoxbit
___________          ___.           __   
\_   _____/______  __\_ |__   _____/  |_ 
 |    __)/  _ \  \/  /| __ \ /  _ \   __\
 |     \(  <_> >    < | \_\ (  <_> )  |  
 \___  / \____/__/\_ \|___  /\____/|__|  
     \/             \/    \/             
*/

const bot = new Discord.Client();
const prefix = botSettings.prefix;
const ver = botSettings.ver;
let count =  bot.guilds.size;
bot.commands = new Discord.Collection();

var servers = {};

const foxservers = [
    "328026252499877889",
    "414143365307826187"
];

process.on('unhandledRejection', console.error)
    //Sets the bot's 'Playing' status
    bot.on('ready', () => {
    bot.user.setActivity(`${bot.guilds.size} Servers | ${ver}`)
  })

    fs.readdir("./commands/", (err, files) => {
    if (err) console.error(err);

    let jsfiles = files.filter(f => f.split(".").pop () === "js");
    if(jsfiles.length <= 0) {
        console.log("No commands have been loaded");
        return;
    }

    console.log(`Loading ${jsfiles.length} commands!`);

    jsfiles.forEach((f, i) => {
    let props = require(`./commands/${f}`);
    console.log(`${i + 1}: ${f} loaded!`);
    bot.commands.set(props.help.name, props);


    });
});


//Censorship
bot.on('message', message => {

    const member = message.member;

    let reason;

    let guild = message.guild;

    var msg = message.content.toLowerCase();
    
    if (message.channel.type !== 'text') {
        return;
    }
    

    var malicious = [
        "pornhub.com",
        "xhamster.com",
        "youareanidiot.com",
        "fthmb.tqn.com",
        "pictureworld.xyz",
        "facebook.co.in",
        "ok.de"
    ];

    var shortlink = [
        "bit.ly",
        "goo.gl",
        "bit.do",
        "tinyurl.com",
        "bitly.com",
        "adf.ly",
        "q.gs",
        "j.mp",
        "u.bb"
    ];

    var dislink = [
        "discord.gg",
        "discordapp.com/invite"
    ];

    var banwords = [
        "fuk",
        "fuc",
        "nigg",
        "fag",
        "cunt",
        "porn",
        "bitch",
	    "pussy",
	    "penis"
    ];

    if (message.content.includes("జ్ఞ‌ా"))
    {
        reason = "Curly Boi";
        message.delete().catch(console.log);
        ban();
    }

    if (malicious.some(word => msg.includes(word)))
    {
        reason = "Malicious Links"
        message.delete().catch(console.log);
        ban();
    }

    if (dislink.some(word => msg.includes(word)))
    {
        reason = "Discord invites";
        if(!message.member.permissions.has("KICK_MEMBERS"))
        {
            reason = "Discord Invite";

            message.delete().catch(console.log);
            censor();
            warning();
            
        }
    }

    if (banwords.some(word => msg.includes(word)))
        {

        
        if(guild.id !== "328026252499877889") 
        {
            return;
            
        }
        

       if(guild.id === "334441603924230144") return;

        reason = "Banned Words";

            message.delete().catch(console.log);
            censor();
        
    }

    if (shortlink.some(word => msg.includes(word)))
        {
        reason = "Short Links";
        if(!message.member.permissions.has('KICK_MEMBERS'))
        {
            message.delete().catch(console.log);
            censor();
        }
    }

    function censor(){

        const modlog = member.guild.channels.find('name', 'mod-log')

        /*
        var embed = new Discord.RichEmbed()
        .addField("You've been warned", `Posting ${reason} are not allowed.`)
        .setColor('DARK_RED')
        .setAuthor('Warning', 'http://www.clker.com/cliparts/H/Z/0/R/f/S/warning-icon-md.png')
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        member.send(embed);
        */

        if(!modlog) return;
        
        var embedlog = new Discord.RichEmbed()
        .setAuthor(`${member.user.username}#${member.user.discriminator}`, member.user.avatarURL)
        .addField("Message Deleted", `${message}`) 
        .addField("Posted by", `${message.member.user}`)
        .addField("Channel", `${message.channel}`)
        .addField("Reason", `${reason}`)
        .setColor('DARK_RED')
        .setFooter(`ID: (${member.user.id})`, `http://www.clker.com/cliparts/H/Z/0/R/f/S/warning-icon-md.png`)
        .setTimestamp()
        bot.channels.get(modlog.id).send(embedlog);

    };

    function warning(){

                const modlog = member.guild.channels.find('name', 'mod-log')

                let warning1 = message.member.guild.roles.find('name', 'Warning #1');
                let warning2 = message.member.guild.roles.find('name', 'Warning #2');
                let warning3 = message.member.guild.roles.find('name', 'Warning #3');
                let warning = "Null";

                if(member.roles.some(r=>["Warning #1"].includes(r.name)) )
                {
                let warning = "Warning #2";
        
                member.addRole(warning2).catch(console.log);
                member.removeRole(warning1).catch(console.log);
        
                warn();
        
                }
        
                else if(member.roles.some(r=>["Warning #2"].includes(r.name)) )
                {
                let warning = "Warning #3";
        
                member.addRole(warning3).catch(console.log);
                member.removeRole(warning2).catch(console.log);
        
                warn();
        
                }

                else if(member.roles.some(r=>["Warning #3"].includes(r.name)) )
                {
                    reason = "Max Warnings";

                    ban();

                }

                else {

                    warning = "Warning #1";

                    member.addRole(warning1).catch(console.log);

                    warn();
                    
                }

            function warn()
                {
                var embed = new Discord.RichEmbed()
                .setAuthor('Warning', 'http://www.clker.com/cliparts/H/Z/0/R/f/S/warning-icon-md.png')
                .setDescription("You have been warned.")
                .addField("Staff Member", `Foxbot Automod`)
                .addField("Reason", `${reason}`)
                .setColor('ffa500')
                .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
                member.send(embed).catch(console.log);
        
                if(!modlog) return;
        
                var embedlog = new Discord.RichEmbed()
                .setAuthor(`${member.user.username}#${member.user.discriminator}`, member.user.avatarURL)
                .addField("User Warned", `${member.user}`)
                .addField("Warning Added", `${warning}`)
                .addField("Staff Member", `${bot.user}`)
                .addField("Reason", `${reason}`)
                .setColor('ffa500')
                .setFooter(`ID: (${member.user.id})`, `http://www.clker.com/cliparts/H/Z/0/R/f/S/warning-icon-md.png`)
                .setTimestamp()
                bot.channels.get(modlog.id).send(embedlog);
        
                }
                
    };

    function ban(){

        if(!member.bannable) return;

        var embed2 = new Discord.RichEmbed()
        .setAuthor("Server Ban", `https://png.icons8.com/ios/2x/hammer-filled.png`)
        .setDescription("You have been banned from Foxbit")
        .addField("Reason", `${reason}`)
        .addField("Time", 'Permanently')
        .addField("Staff Member", `Foxbot Automod`)
        .setColor("DARK_RED")
        .addBlankField()
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        member.send(embed2).catch(console.log);

        setTimeout(function (){

            member.ban(reason)
          
          }, 500);

        const modlog = member.guild.channels.find('name', 'mod-log')

        if(!modlog) return;

        var embedlog = new Discord.RichEmbed()
        .setAuthor(`${member.user.username}#${member.user.discriminator}`, member.user.avatarURL)
        .addField("User Banned", `${member.user}`)
        .addField("Staff Member", `${bot.user}`)
        .addField("Message Deleted", `${message}`)
        .addField("Reason", `${reason}`)
        .setColor('DARK_RED')
        .setFooter(`ID: (${member.user.id})`, `https://png.icons8.com/ios/2x/hammer-filled.png`)
        .setTimestamp()
        return bot.channels.get(modlog.id).send(embedlog);

    };
    });

    function play(connection, message) {

    var server = servers[message.guild.id];

    server.dispatcher = connection.playStream(YTDL(server.queue[0], {filter: "audioonly"}));

    server.queue.shift();

    server.dispatcher.on("end", function() {
        if (server.queue[0]) play(connection, message);
        else connection.disconnect();


    });
    };

//Starts the bot's async message listener to listen for commands.


function play(connection, message) {

    var server = servers[message.guild.id];

    server.dispatcher = connection.playStream(YTDL(server.queue[0], {filter: "audioonly"}));

    server.queue.shift();

    server.dispatcher.on("end", function() {
        if (server.queue[0]) play(connection, message);
        else connection.disconnect();


    });
};

bot.on("message", async message => {

    if (message.channel.type !== 'text') return;
    if (message.author.bot) return;
    if (!message.content.startsWith(botSettings.prefix)) return;

    //const args = message.content.slice(botSettings.prefix.length).split(' ');

    let messageArray = message.content.split(/\s+/g);
    let command = messageArray[0];
    let args = messageArray.slice(1);

    let cmd = bot.commands.get(command.slice(prefix.length));
    if(cmd) cmd.run(bot, message, args);

    let member = message.member;
    let mention = message.mentions.members.first();
    const modlog = message.guild.channels.find('name', 'mod-log');
    const feedlog = message.guild.channels.find('name', 'feedback');
    const agree = message.guild.channels.find('name', 'agree');
    const checkpoint = message.member.guild.roles.find('name', 'Checkpoint');

    var args2 = message.content.substring(prefix.length).split(" ");

    
    /*switch (args2[0].toLowerCase()) {
        case "setrules":
            if(!message.member.roles.some(r=>["⚙️ Admin"].includes(r.name)) )
            {
                var embed = new Discord.RichEmbed()
                    .addField(":x: No permission", "You do not have permission to use this command.")
                    .setColor('DARK_RED')
                return message.channel.send(embed);
            }
            

            else {
                var embed = new Discord.RichEmbed()
                    .setAuthor('Foxbit Official Rules & Info', message.guild.iconURL)
                    .addBlankField
                    .set
                    .setColor('DARK_RED')
                return message.channel.send(embed);

            }
            break;
        
        case "specs":
            //WIP - Will add MySQL later on

            const Ryzen = bot.emojis.get("404786759222689803");
            const RX = bot.emojis.get("411616107523342338");

            var embed = new Discord.RichEmbed()
            .setAuthor(`Foxbit's PC Specs`, message.guild.owner.user.avatarURL)
            .addField(`Created On`, `N/A`)
            .addField("CPU", `${Ryzen} Ryzen 5 1600X @ 4.0GHz`)
            .addField("GPU", `${RX} Radeon RX 480 8GB`)
            .addField("Mobo", `MSI X370 Gaming Pro Carbon`)
            .addField("RAM", `G.SKILL Trident Z RGB 16GB DDR4 @ 3200MHz`)
            .addField("SSD", `SanDisk Plus 120GB`)
            .addField("HDD", `Seagate Momentus XT 500GB`)
            .addField("Case", `Phanteks P400S`)
            .setColor('DARK_RED')
            message.channel.send(embed);
            break:
            
        case "play":
            let member = message.member;
    
            if (!args2[1]) {
            var embed = new Discord.RichEmbed()
            .addField(":x: Error", "Please provide a link.")
            return message.channel.send(embed);
            }
    
            if (!member.voiceChannel) {
            var embed = new Discord.RichEmbed()
            .addField(":x: Error", "You aren't connected to a voice channel.")
            return message.channel.send(embed);
            }
    
            if(!servers[message.guild.id]) servers[message.guild.id] = {
            queue: []
        };
    
        var server = servers[message.guild.id];
    
        server.queue.push(args2[0]);
    
        if (!message.guild.voiceConnection) member.voiceChannel.join().then(function(connection) {
            play(connection, message);
        });
    
        var embed = new Discord.RichEmbed()
        .setAuthor("Now Playing", "https://png.icons8.com/windows/1600/musical-notes.png")
        .setTitle(`${args2[0]}`)
        .setURL(`${args2[0]}`)
        .setImage(`${args2[0]}`)
        .setColor("BLUE")
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        message.channel.send(embed);
        break;
        */

        



        
        });






//Welcome message for member joining
bot.on('guildMemberAdd', function(member) {
    let channel;
    
    if (member.guild.channels.find('name', 'hello-goodbye'))
    {
        channel = member.guild.channels.find('name', 'hello-goodbye')
    }
    
    else if (member.guild.channels.find('name', 'bienvenido'))
    {
        channel = member.guild.channels.find('name', 'bienvenido')
    }

    if(!channel) return;



    //Does not currently work.
    /*
    if(member.user.createdTimestamp <= Date.now())
    {
        var embed = new Discord.RichEmbed()
        .setAuthor(`${member.user.username}#${member.user.discriminator}`, member.user.avatarURL)
        .addField("User has joined", `Welcome to the server, ${member}`)
        .setColor('BLUE')
        .setFooter(`ID: (${member.id})`, `https://png.icons8.com/ios/540/about.png`)
        .addBlankField()
        .setTitle(":warning: User was created within 24 hours")
        .setTimestamp()
        .setThumbnail(member.user.avatarURL)
    channel.send(embed);
    }
    else;
    */

    var embed = new Discord.RichEmbed()
        .setAuthor(`${member.user.username}#${member.user.discriminator}`, member.user.avatarURL)
        .addField("User has joined", `Welcome to the server, ${member}`)
        .setColor('BLUE')
        .setFooter(`ID: (${member.id})`, `https://png.icons8.com/ios/540/about.png`)
        .setTimestamp()
        .setThumbnail(member.user.avatarURL)
    channel.send(embed);


    let role = member.guild.roles.find("name", "Checkpoint");

    if(!role) return;

    member.addRole(role).catch(console.error);
    
});

//Ban message for banned member
bot.on('guildBanAdd', function(guild, user) {

    let channel;

    if (member.guild.channels.find('name', 'hello-goodbye'))
    {
        channel = member.guild.channels.find('name', 'hello-goodbye')
    }
    
    else if (member.guild.channels.find('name', 'bienvenido'))
    {
        channel = member.guild.channels.find('name', 'bienvenido')
    }

    if (!channel) return;

    var embed = new Discord.RichEmbed()
        .setAuthor(`${user.username}#${user.discriminator}`, user.avatarURL)
        .addField("User has been banned", `Ban hammer has been slammed on ${user}`)
        .setColor('DARK_RED')
        .setFooter(`ID: (${user.id})`, `https://png.icons8.com/ios/invite2x/hammer-filled.png`)
        .setTimestamp()
        .setThumbnail(user.avatarURL)
    channel.send(embed);
});

//Leave message for member leaving
bot.on('guildMemberRemove', function(member) {
    const channel = member.guild.channels.find('name', 'hello-goodbye');

    if(!channel) return;

    var embed = new Discord.RichEmbed()
        .setAuthor(`${member.user.username}#${member.user.discriminator}`, member.user.avatarURL)
        .addField("User has left", `We'll miss you, ${member}`)
        .setColor('WHITE')
        .setFooter(`ID: (${member.id})`, `https://png.icons8.com/ios/540/about.png`)
        .setTimestamp()
        .setThumbnail(member.user.avatarURL)
    channel.send(embed);
});

//Log when guild add
bot.on('guildCreate', function(guild) {

    const botlog = bot.channels.find('id', '428250953804677121')

    guild.fetchInvites()
    .then(invites =>{

    Array.from(invites).every(function(invite) {
        var inv = "https://discord.gg/" + (Object.values(invite)[0]);
        var inv2 = "https://discord.gg/" + (Object.values(invite)[1]);
        var embed = new Discord.RichEmbed()
    .setAuthor(`${guild.name}`, `${guild.iconURL}`)
    .setTitle(`Bot Added`)
    .addField(`Server ID`,`${guild.id}`)
    .addField(`Invite`,`${inv}`)
    .setURL(`${inv}`)
    .addField('Owner', `${guild.owner.user.username}#${guild.owner.user.discriminator} ${guild.owner}`)
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    .setTimestamp()
    .setThumbnail(guild.iconURL)
    bot.channels.get(botlog.id).send(embed).catch(console.log);

    bot.channels.get(botlog.id).send(`${inv}`).catch(console.log);


    });

    })

    .catch(console.error);

});

/*
bot.on('messageDelete', message =>  {

    let member = message.member;

    const modlog = message.guild.channels.find('name', 'mod-log');

    if(!modlog) return;

    var embedlog = new Discord.RichEmbed()
    .setAuthor(`${member.user.username}#${member.user.discriminator}`, member.user.avatarURL)
    .addField("Message Deleted", `${message}`)
    .addField("Posted by", `${message.member.user}`)
    .addField("Channel", `${message.channel}`)
    .setColor('DARK_RED')
    .setFooter(`ID: (${member.user.id})`, `http://www.clker.com/cliparts/H/Z/0/R/f/S/warning-icon-md.png`)
    .setTimestamp()
    bot.channels.get(modlog.id).send(embedlog).catch(console.log);

});
*/

bot.on("ready", function(message) {
    console.log("-------------------------------------------")
    console.log("Logged in succefully to Discord API.");
    console.log("-------------------------------------------")
    console.log(`${bot.user.username} is online. Version ${ver} initiated.`); // Tells that bot is ready
    console.log("-------------------------------------------")
    console.log(`COPYRIGHT © Foxbit#6845 - @itsfoxbit`);
    console.log("-------------------------------------------")
    console.log(",------.               ,--.   ,--.  ,--.   ");
    console.log("|  .---',---.,--. ,--. |  |-. `--',-'  '-. ");
    console.log("|  `--,| .-. |\  `'   / | .-. ',--.'-.  .-' ");
    console.log("|  |`  ' '-' '/  /.  \  | `-' ||  |  |  |   ");
    console.log("`--'    `---''--' '--'  `---' `--'  `--'  ");
    console.log("-------------------------------------------")

    /*
    const botdump = bot.channels.find('id', '442916109331464212')

    var embed = new Discord.RichEmbed()
        .setAuthor(`ModLog`, `https://png.icons8.com/ios/540/about.png`)
        .addField(`Startup`,`FoxBot is online. Version ${ver} initiated.`)
        .setColor('WHITE')
        .setFooter(`${bot.user.username}`, bot.user.avatarURL)
        .setTimestamp()
        .setThumbnail(bot.user.avatarURL)

    bot.channels.get(botdump.id).send("-------------------------------------------")
    bot.channels.get(botdump.id).send("Logged in succefully to Discord API.");
    bot.channels.get(botdump.id).send("-------------------------------------------")
    bot.channels.get(botdump.id).send(`${bot.user.username} is online. Version ${ver} initiated.`); // Tells that bot is ready
    bot.channels.get(botdump.id).send("-------------------------------------------")
    bot.channels.get(botdump.id).send(`COPYRIGHT © Foxbit#6845 - @itsfoxbit`);
    */
});

bot.login(botSettings.token) // Get token from botsettings.json
