const Discord = require("discord.js");
const botSettings = require("../botsettings.json");
const YTDL = require("ytdl-core");

const ver = botSettings.ver;
    
var servers = {};

function play(connection, message) {

    var server = servers[message.guild.id];

    server.dispatcher = connection.playStream(YTDL(server.queue[0], {filter: "audioonly"}));

    server.queue.shift();

    server.dispatcher.on("end", function() {
        if (server.queue[0]) play(connection, message);
        else connection.disconnect();


    });
};

module.exports.run = async (bot, message, args) => {

    let member = message.member;

    
    if (!args[0]) {
        var embed = new Discord.RichEmbed()
        .addField(":x: Error", "Please provide a link.")
        return message.channel.send(embed);
    }

    if (!member.voiceChannel) {
        var embed = new Discord.RichEmbed()
        .addField(":x: Error", "You aren't connected to a voice channel.")
        return message.channel.send(embed);
    }

    if(!servers[message.guild.id]) servers[message.guild.id] = {
        queue: []
    };

    var server = servers[message.guild.id];

    server.queue.push(args[0]);

    if (!message.guild.voiceConnection) member.voiceChannel.join().then(function(connection) {
        play(connection, message);
    });

    var embed = new Discord.RichEmbed()
    .setAuthor("Now Playing", "https://png.icons8.com/windows/1600/musical-notes.png")
    .setTitle(`${args[0]}`)
    .setURL(`${args[0]}`)
    .setImage(`${args[0]}`)
    .setColor("BLUE")
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    message.channel.send(embed);
       
}

module.exports.help = {

    name: "play-noload"

}