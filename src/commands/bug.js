const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {

    const reportlog = bot.channels.find('id', "443291471378907158")
    let member = message.member;
    var guild = message.guild;
    var report = args.slice().join(' ');
    
    
    if(!reportlog)
    {
        var embed = new Discord.RichEmbed()
        .setTitle(":x: Reporting is not currently enabled.")
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        return message.channel.send(embed);
    }
    

    if(!report)
    {
        var embed = new Discord.RichEmbed()
        .setTitle(":x: Please provide a report.")
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        return message.channel.send(embed);
    }

    var embedreport = new Discord.RichEmbed()
    .setAuthor(`${member.user.username}#${member.user.discriminator}`, member.user.avatarURL)
    .addField("Report", `${report}`)
    .addField("Posted by", `${message.member.user} (${message.member.user.id})`)
    .addField("Server", `${message.member.guild.name} (${message.member.guild.id})`)
    .setColor('BLUE')
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    .setTimestamp()
    bot.channels.get(reportlog.id).send(embedreport).then(message => {
        message.react("✔"), message.react("❌") });
    

    var embed = new Discord.RichEmbed()
    .addField(`:white_check_mark: Your response has been recorded.`, `Thanks, ${member.user}`)
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    message.channel.send(embed);

    message.delete();
       
}

module.exports.help = {

    name: "report"

}