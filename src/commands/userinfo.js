const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {
    
    let member = message.mentions.members.first();

    if(!member)
    var embed = new Discord.RichEmbed()
    .addField(":x: Invalid format", `Please mention a user.`)

    else 
    var embed = new Discord.RichEmbed()
    .setAuthor(`${member.user.tag}`, member.user.avatarURL)
    .addField(`ID`, `${member.user.id}`)
    .addField("Join date", ` ${member.joinedAt}`)
    .addField("User created", ` ${member.user.createdAt}`)
    .setColor('BLUE')
    .setThumbnail(member.user.avatarURL)
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    message.channel.send(embed);
       
}

module.exports.help = {

    name: "userinfo"

}