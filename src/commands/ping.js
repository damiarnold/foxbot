const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {

    bot.user.setUsername("Foxbot");
    
    if (bot.ping <= 100) {
        var embed = new Discord.RichEmbed()
        .addField(":white_check_mark: Status", `FoxBot is online. ${bot.pings[0]}ms`)
        .setColor("BLUE")
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        message.channel.send(embed);
        
        }
        else {
        var embed = new Discord.RichEmbed()
        .addField(":warning:  Status", `FoxBot is online. ${bot.pings[0]}ms`)
        .setColor("DARK_RED")
        .addBlankField()
        .setDescription("Latency appears to be high at the moment.")
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        message.channel.send(embed);
        
        }
       
}

module.exports.help = {

    name: "ping"

}