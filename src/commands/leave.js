const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {

    let member = message.member;
    let mention = message.mentions.members.first();
    
    if(member.id === "192776246185164802") 
    {
    return message.guild.leave();
    }
    var embed = new Discord.RichEmbed()
    .addField(":x: No permission", "You do not have permission to use this command.")
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    .setColor('DARK_RED')
    message.channel.send(embed);
        
}

module.exports.help = {

    name: "leave"

}