const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {

    let member = message.member;
    let guild = message.member.guild;
    let rules = message.member.guild.channels.find('name', 'rules-info')
    const modlog = message.guild.channels.find('name', 'mod-log');
    const feedlog = message.guild.channels.find('name', 'feedback');
    const agree = message.guild.channels.find('name', 'agree');
    const heybye = message.guild.channels.find('name', 'hello-goodbye');
    const checkpoint = message.member.guild.roles.find('name', 'Checkpoint');
    


    if(!message.member.permissions.has('BAN_MEMBERS')){
    var embed = new Discord.RichEmbed()
        .addField(":x: No permission", "You do not have permission to use this command.")
        .setColor('DARK_RED')
    return message.channel.send(embed);
    }


    if(!modlog)
    {
    message.guild.createChannel('mod-log');
    /*    
    const modlog = message.guild.channels.find('name', 'mod-log');
        var embed = new Discord.RichEmbed()
        .addField(":white_check_mark: Success", `${modlog} has been created.`)
        .setColor("BLUE")
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        message.channel.send(embed);
        */
    }
    else
    {
    var embed = new Discord.RichEmbed()
    .addField(":x: Error", `${modlog} already exists.`)
    .setColor("DARK_RED")
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    message.channel.send(embed);
    }

    if(!feedlog)
    {
    message.guild.createChannel('feedback');

    /*
        const feedlog = message.guild.channels.find('name', 'feedback');

        var embed = new Discord.RichEmbed()
        .addField(":white_check_mark: Success", `${feedlog} has been created.`)
        .setColor("BLUE")
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        message.channel.send(embed);
        */
    
    }
    else
    {
    var embed = new Discord.RichEmbed()
    .addField(":x: Error", `${feedlog} already exists.`)
    .setColor("DARK_RED")
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    message.channel.send(embed);
    }

    if(!checkpoint)
    {
    message.guild.createRole({
        name: 'Checkpoint',
        color: 'DARK_RED',
      });

      /*
        var embed = new Discord.RichEmbed()
        .addField(":white_check_mark: Success", `Checkpoint has been created.`)
        .setColor("BLUE")
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        message.channel.send(embed);
        */
    }
    else
    {
    var embed = new Discord.RichEmbed()
    .addField(":x: Error", `Checkpoint already exists.`)
    .setColor("DARK_RED")
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    message.channel.send(embed);
    }

    if(!agree)
    {
    message.guild.createChannel('agree');
        

        //const agree = message.guild.channels.find('agree');

        /*
        var embed = new Discord.RichEmbed()
        .addField(":white_check_mark: Success", `${agree} has been created.`)
        .setColor("BLUE")
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        message.channel.send(embed);
        */
        
    }
    else
    {
    var embed = new Discord.RichEmbed()
    .addField(":x: Error", `${agree} already exists.`)
    .setColor("DARK_RED")
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    message.channel.send(embed);
    }

    if(!heybye)
    {
    message.guild.createChannel('hello-goodbye');
        

        //const agree = message.guild.channels.find('agree');

        /*
        var embed = new Discord.RichEmbed()
        .addField(":white_check_mark: Success", `${agree} has been created.`)
        .setColor("BLUE")
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        message.channel.send(embed);
        */
        
    }
    else
    {
    var embed = new Discord.RichEmbed()
    .addField(":x: Error", `${heybye} already exists.`)
    .setColor("DARK_RED")
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    message.channel.send(embed);
    }

    var embed = new Discord.RichEmbed()
    .setTitle("Setup Successful")
    .setColor('BLUE')
    .setTimestamp()
    message.channel.send(embed)

}


module.exports.help = {

    name: "setup"

}