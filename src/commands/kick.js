const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {

    let member = message.member;
    let mention = message.mentions.members.first();
    let reason = args.slice(1).join(' ');
    const modlog = message.guild.channels.find('name', 'mod-log');
    
    if(!message.member.permissions.has('KICK_MEMBERS')){
    var embed = new Discord.RichEmbed()
        .addField(":x: No permission", "You do not have permission to use this command.")
        .setColor('DARK_RED')
    return message.channel.send(embed);
    }

    if(!message.guild.member(bot.user).permissions.has('KICK_MEMBERS')){
        var embed = new Discord.RichEmbed()
        .addField(":x: No permission", "I do not have permission to perform this action.")
        .setColor('DARK_RED')
        return message.channel.send(embed);
    }

    if(!mention) {
        var embed = new Discord.RichEmbed()
        .addField(":x: Invalid format", "Please choose a user to be kicked.")
        return message.channel.send(embed);
    }
    if(!mention.kickable) {
        
        var embed = new Discord.RichEmbed()
        .addField(":x: Invalid format", "Member cannot be kicked.")
        return message.channel.send(embed);
    }

    
    if(!reason) {;
        var embed = new Discord.RichEmbed()
        .addField(":x: Invalid format", "Please choose a reason for the kick.")
        return message.channel.send(embed);
    }

    var embed2 = new Discord.RichEmbed()
    .addField(":hammer: Server Kick", `You have been kicked from Foxbit`)
    .addField("Reason", `${reason}`)
    .addField("Staff Member", `${message.author.tag}`)
    .setColor("DARK_RED")
    .addBlankField()
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    mention.send(embed2).catch(console.log);

    var embed = new Discord.RichEmbed()
    .addField(":hammer: User Kicked", `${mention.user.tag} has been kicked.`)
    .addField("Reason", `${reason}`)
    .addField("Staff Member", `${message.member.user}`)
    .setColor("DARK_RED")
    .addBlankField()
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    message.channel.send(embed);

    setTimeout(function (){

        message.delete()
        mention.kick(reason)
      
      }, 500);


      if(!modlog) return;

      var embedlog = new Discord.RichEmbed()
      .setAuthor(`${member.user.username}#${member.user.discriminator}`, member.user.avatarURL)
      .addField("User Kicked", `${mention.user}`)
      .addField("Kicked by", `${message.member.user}`)
      .addField("Reason", `${reason}`)
      .setColor('DARK_RED')
      .setFooter(`ID: (${mention.user.id})`, `https://png.icons8.com/ios/2x/hammer-filled.png`)
      .setTimestamp()
      bot.channels.get(modlog.id).send(embedlog)
       
}

module.exports.help = {

    name: "kick"

}