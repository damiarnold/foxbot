const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {
    
    let guild = message.member.guild;
    let rules = message.member.guild.channels.find('name', 'rules-info')
    let wave = bot.emojis.get("404788181955772416");
    let channel = args.slice(0).join(' ');

    if(!message.member.permissions.has('KICK_MEMBERS'))
    {
    var embed = new Discord.RichEmbed()
        .addField(":x: No permission", "You do not have permission to use this command.")
        .setColor('DARK_RED')
    return message.channel.send(embed);
    }

    if(!channel) {
        var embed = new Discord.RichEmbed()
        .addField(":x: Error", "Please provide a channel after the command.")
        return message.channel.send(embed);
    }

    else
    {
    var embed = new Discord.RichEmbed()
    .addField(`${wave} Welcome to ${guild.name}`, `Please read through ${channel}`)
    .addBlankField()
    .addField("Access", "Type !agree when finished to gain access to the server!")
    .setColor('BLUE')
    .setAuthor(`${guild.name}`, `${guild.iconURL}`)
    .setFooter(`${bot.user.username} ${ver}`, `${bot.user.avatarURL}`)
    .setThumbnail(`${guild.iconURL}`)
    message.channel.send(embed);

    }

    message.delete();
       
}

module.exports.help = {

    name: "welcome"

}