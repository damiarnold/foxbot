const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {
    
    if(!message.member.permissions.has('KICK_MEMBERS')){
        var embed = new Discord.RichEmbed()
            .addField(":x: No permission", "You do not have permission to use this command.")
            .setColor('DARK_RED')
        return message.channel.send(embed);
        }

        const amount = parseInt(args[0]) + 1;

        if (isNaN(amount)){
        var embed = new Discord.RichEmbed()
        .addField(":x: Error", "Use an integer.")
        return message.channel.send(embed);
        }
        else if (amount <= 1 || amount > 100) {
        var embed = new Discord.RichEmbed()
        .addField(":x: Error", "Choose a number between 2 and 100.")
        return message.channel.send(embed);
        }

        message.channel.bulkDelete(amount, true).catch(err => {
        var embed = new Discord.RichEmbed()
        .addField(":x: Error", "Error purging messages!")
        .setColor("DARK_RED")
        message.channel.send(embed);
        });
       
}

module.exports.help = {

    name: "purge"

}