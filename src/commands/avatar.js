const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {

    let member = message.member;
    let mention = message.mentions.members.first();
    
    if(!mention) {
        var embed =  new Discord.RichEmbed()
        .setAuthor(member.user.username, member.user.avatarURL)
        .setDescription(`[${member.user.username}'s avatar](${member.user.avatarURL})`)
        .setImage(member.user.avatarURL)
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        .setColor('RED')
        return message.channel.send(embed);
        }

        var embed =  new Discord.RichEmbed()
        .setAuthor(mention.user.username, mention.user.avatarURL)
        .setDescription(`[${mention.user.username}'s avatar](${mention.user.avatarURL})`)
        .setImage(mention.user.avatarURL)
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        .setColor('RED')
        message.channel.send(embed);
        
}

module.exports.help = {

    name: "avatar"

}