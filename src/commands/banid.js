const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {

    let member = message.member;
    let mention = message.mentions.members.first();
    let reason = args.slice(1).join(' ');
    let guildban = message.guild;
    let banID = args[0];
    const modlog = message.guild.channels.find('name', 'mod-log');

    if(!member.permissions.has('BAN_MEMBERS')){
    var embed = new Discord.RichEmbed()
        .addField(":x: No permission", "You do not have permission to use this command.")
        .setColor('DARK_RED')
        return message.channel.send(embed);
    }

    if(!message.guild.member(bot.user).permissions.has('BAN_MEMBERS')){
        var embed = new Discord.RichEmbed()
        .addField(":x: No permission", "I do not have permission to perform this action.")
        .setColor('DARK_RED')
        return message.channel.send(embed);
    }

    if(!banID) {
        var embed = new Discord.RichEmbed()
        .addField(":x: Error", "Please type in a User ID to be banned.")
        return message.channel.send(embed);
    }

    if(!reason) {
        var embed = new Discord.RichEmbed()
        .addField(":x: Error", "Please choose a reason for the ban.")
        return message.channel.send(embed);
    }

    guildban.ban(banID, reason).catch(err => {
        var embed = new Discord.RichEmbed()
        .addField(":x: Error", "Error banning ID.")
        .setColor("DARK_RED")
        return message.channel.send(embed);
    })

        var embed = new Discord.RichEmbed()
        .setAuthor("ID Banned", `https://png.icons8.com/ios/2x/hammer-filled.png`)
        .addField("Reason", `${reason}`)
        .addField("Staff Member", `${message.member.user}`)
        .setColor("DARK_RED")
        .addBlankField()
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        message.channel.send(embed);

        var embedlog = new Discord.RichEmbed()
        .setAuthor(`ID Ban`, `https://png.icons8.com/ios/2x/hammer-filled.png`)
        .addField("User", `<@${banID}>`)
        .addField("ID Banned", `${banID}`)
        .addField("Reason", `${reason}`)
        .addField("Banned by", `${message.member.user}`)
        .setColor('DARK_RED')
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        .setTimestamp()
        bot.channels.get(modlog.id).send(embedlog);
       
}

module.exports.help = {

    name: "banid"

}