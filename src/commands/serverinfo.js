const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {
    
    var embed = new Discord.RichEmbed()
    .setAuthor(message.guild.name, message.guild.iconURL)
    .addField(`ID`, `${message.guild.id}`)
    .addField("Server Owner", ` ${message.guild.owner.user} (${message.guild.ownerID})`)
    .addField("Verification Level", `${message.guild.verificationLevel}`)
    .addField("Created On", ` ${message.guild.createdAt}`)
    .addField("Region", ` ${message.guild.region}`)
    .addField("Members", ` ${message.guild.memberCount}`)
    .addBlankField()
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    .setColor('BLUE')
    .setThumbnail(message.guild.iconURL) 
    message.channel.send(embed);
       
}

module.exports.help = {

    name: "serverinfo"

}