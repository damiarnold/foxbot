const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {

    const feedlog = message.guild.channels.find('name', 'feedback');
    let member = message.member;
    var guild = message.guild;
    var feedback = args.slice().join(' ');
    
    if(!feedlog)
    {
        var embed = new Discord.RichEmbed()
        .setTitle(":x: Feedback is not enabled.")
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        return message.channel.send(embed);
    }

    if(!feedback)
    {
        var embed = new Discord.RichEmbed()
        .setTitle(":x: Please have feedback.")
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        return message.channel.send(embed);
    }

    var embedfeed = new Discord.RichEmbed()
    .setAuthor(`${member.user.username}#${member.user.discriminator}`, member.user.avatarURL)
    .addField("Feedback", `${feedback}`)
    .addField("Posted by", `${message.member.user}`)
    .setColor('BLUE')
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    .setTimestamp()
    bot.channels.get(feedlog.id).send(embedfeed).then(message => {
        message.react("👍"), message.react("👎") });
    

    var embed = new Discord.RichEmbed()
    .addField(`:white_check_mark: Your response has been recorded.`, `Thanks, ${member.user}`)
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    message.channel.send(embed);

    message.delete();
       
}

module.exports.help = {

    name: "feedback"

}