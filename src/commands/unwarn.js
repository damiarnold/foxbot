const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {
    
    let warning1 = message.member.guild.roles.find('name', 'Warning #1');
    let warning2 = message.member.guild.roles.find('name', 'Warning #2');
    let warning3 = message.member.guild.roles.find('name', 'Warning #3');
    let warning = "Warning #1";
    let reason = args.slice(1).join(' ');
    let member = message.member;
    let mention = message.mentions.members.first();
    const modlog = message.guild.channels.find('name', 'mod-log');

    if(!member.permissions.has('KICK_MEMBERS')){
        var embed = new Discord.RichEmbed()
            .addField(":x: No permission", "You do not have permission to use this command.")
            return message.channel.send(embed);
        }

    if(!mention) {
        var embed = new Discord.RichEmbed()
        .addField(":x: Error", "Please choose a user to be warned.")
        return message.channel.send(embed);
        }

    if(!reason) {
        var embed = new Discord.RichEmbed()
        .addField(":x: Error", "Please choose a reason for the warn.")
        return message.channel.send(embed);
        }

    if(mention.roles.some(r=>["Warning #1"].includes(r.name)) )
        {
            let warning = "Warning #1";

            mention.removeRole(warning1).catch(console.log);

            return unwarn();

        }

    if(mention.roles.some(r=>["Warning #2"].includes(r.name)) )
        {
            let warning = "Warning #2";

            mention.removeRole(warning2).catch(console.log);
                    mention.addRole(warning1).catch(console.log);

            return unwarn();

        }

    if(mention.roles.some(r=>["Warning #3"].includes(r.name)) )
        {
            let warning = "Warning #3";

            mention.removeRole(warning3).catch(console.log);
            mention.addRole(warning2).catch(console.log);

            return unwarn();

        }


    
    else {
        var embed = new Discord.RichEmbed()
            .addField(":x: Error", "This user has no warnings.")
            message.channel.send(embed);
        };

    function unwarn()
        {
        var embed = new Discord.RichEmbed()
        .setAuthor('Un-Warned', 'http://www.clker.com/cliparts/H/Z/0/R/f/S/warning-icon-md.png')
        .setDescription("You have been un-warned.")
        .addField("Staff Member", `${message.member.user}`)
        .addField("Reason", `${reason}`)
        .setColor('BLUE')
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        mention.send(embed).catch(console.log);

        var embed = new Discord.RichEmbed()
        .setAuthor("User Un-Warned", "http://www.clker.com/cliparts/H/Z/0/R/f/S/warning-icon-md.png")
        .setDescription(`${mention.user} has been warned.`)
        .addField("Staff Member", `${message.member.user}`)
        .addField("Reason", `${reason}`)
        .setColor('BLUE')
        .addBlankField()
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        message.channel.send(embed);

        if(!modlog) return;

        var embedlog = new Discord.RichEmbed()
        .setAuthor(`${mention.user.username}#${mention.user.discriminator}`, mention.user.avatarURL)
        .addField("User Un-Warned", `${mention.user}`)
        .addField("Warning Removed", `${warning}`)
        .addField("Staff Member", `${message.member.user}`)
        .addField("Reason", `${reason}`)
        .setColor('BLUE')
        .setFooter(`ID: (${mention.user.id})`, `http://www.clker.com/cliparts/H/Z/0/R/f/S/warning-icon-md.png`)
        .setTimestamp()
        return bot.channels.get(modlog.id).send(embedlog);


        }

       
}

module.exports.help = {

    name: "unwarn"

}