const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {

    let member = message.member;
    let mention = message.mentions.members.first();
    let reason = args.slice(1).join(' ');
    const modlog = message.guild.channels.find('name', 'mod-log');

    if(!member.permissions.has('BAN_MEMBERS')){
        var embed = new Discord.RichEmbed()
        .addField(":x: No permission", "You do not have permission to use this command.")
        .setColor('DARK_RED')
        return message.channel.send(embed);
    }

    if(!message.guild.member(bot.user).permissions.has('BAN_MEMBERS')){
        var embed = new Discord.RichEmbed()
        .addField(":x: No permission", "I do not have permission to perform this action.")
        .setColor('DARK_RED')
        return message.channel.send(embed);
    }

    if(!mention) {
        var embed = new Discord.RichEmbed()
        .addField(":x: Error", "Please choose a user to be banned.")
        return message.channel.send(embed);
    }

    if(!reason) {
        var embed = new Discord.RichEmbed()
        .addField(":x: Error", "Please choose a reason for the ban.")
        return message.channel.send(embed);
    }

    if(!mention.bannable) {
        var embed = new Discord.RichEmbed()
        .addField(":x: Error", "Member cannot be banned.")
        return message.channel.send(embed);
    }

    var embed2 = new Discord.RichEmbed()
    .setAuthor("Server Ban", `https://png.icons8.com/ios/2x/hammer-filled.png`)
    .setDescription("You have been banned from Foxbit")
    .addField("Reason", `${reason}`)
    .addField("Time", 'Permanently')
    .addField("Staff Member", `${message.author.tag}`)
    .setColor("DARK_RED")
    .addBlankField()
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    mention.send(embed2).catch(console.log);
    
    var embed = new Discord.RichEmbed()
    .addField(":hammer: User Banned", `${mention.user.tag} has been banned.`)
    .addField("Reason", `${reason}`)
    .addField("Staff Member", `${message.member.user}`)
    .setColor("DARK_RED")
    .addBlankField()
    .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
    message.channel.send(embed);

    setTimeout(function (){

        message.delete()
        mention.ban(reason)
      
      }, 500);

      if(!modlog) return;
    
        var embedlog = new Discord.RichEmbed()
        .setAuthor(`${mention.user.username}#${mention.user.discriminator}`, mention.user.avatarURL)
        .addField("User Banned", `${mention.user}`)
        .addField("Banned by", `${message.member.user}`)
        .addField("Reason", `${reason}`)
        .setColor('DARK_RED')
        .setFooter(`ID: (${mention.user.id})`, `https://png.icons8.com/ios/2x/hammer-filled.png`)
        .setTimestamp()
        bot.channels.get(modlog.id).send(embedlog);
       
}

module.exports.help = {

    name: "ban"

}