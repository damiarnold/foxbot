const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {
    
    let warning1 = message.member.guild.roles.find('name', 'Warning #1');
    let warning2 = message.member.guild.roles.find('name', 'Warning #2');
    let warning3 = message.member.guild.roles.find('name', 'Warning #3');
    let warning = "Null";
    let reason = args.slice(1).join(' ');
    let member = message.member;
    let mention = message.mentions.members.first();
    const modlog = message.guild.channels.find('name', 'mod-log');

    if(!member.permissions.has('KICK_MEMBERS')){
        var embed = new Discord.RichEmbed()
            .addField(":x: No permission", "You do not have permission to use this command.")
            return message.channel.send(embed);
        }

    if(!mention) {
        var embed = new Discord.RichEmbed()
        .addField(":x: Error", "Please choose a user to be warned.")
        return message.channel.send(embed);
        }

    if(!reason) {
        var embed = new Discord.RichEmbed()
        .addField(":x: Error", "Please choose a reason for the warn.")
        return message.channel.send(embed);
        }

    if(mention.roles.some(r=>["Warning #1"].includes(r.name)) )
        {
        let warning = "Warning #2";

        mention.addRole(warning2).catch(console.log);
        mention.removeRole(warning1).catch(console.log);

        warn();

        }

    else if(mention.roles.some(r=>["Warning #2"].includes(r.name)) )
        {
        let warning = "Warning #3";

        mention.addRole(warning3).catch(console.log);
        mention.removeRole(warning2).catch(console.log);

        warn();

        }

    else if(mention.roles.some(r=>["Warning #3"].includes(r.name)) )
        {
            if(!mention.bannable) {
                var embed = new Discord.RichEmbed()
                .addField(":x: Error", "Member cannot be banned.")
                return message.channel.send(embed);
            }

            mention.ban();

            var embed2 = new Discord.RichEmbed()
            .setAuthor("Server Ban", "https://png.icons8.com/ios/2x/hammer-filled.png")
            .setDescription("You have been banned from Foxbit")
            .addField("Reason", `Max Warnings`)
            .addField("Time", 'Permanently')
            .addField("Staff Member", `${message.member.user}`)
            .setColor("DARK_RED")
            .addBlankField()
            .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
            mention.send(embed2).catch(console.log);

            var embed = new Discord.RichEmbed()
            .setAuthor("User Banned", "https://png.icons8.com/ios/2x/hammer-filled.png")
            .setDescription(`${mention.user}} has been banned.`)
            .addField("Reason", `${reason}`)
            .addField("Staff Member", `${message.member.user}`)
            .setColor("DARK_RED")
            .addBlankField()
            .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
            message.channel.send(embed);

            if(!modlog) return;
            
            var embedlog = new Discord.RichEmbed()
            .setAuthor(`${mention.user.username}#${mention.user.discriminator}`, mention.user.avatarURL)
            .addField("User Banned", `${mention.user}`)
            .addField("Staff Member", `${message.member.user}`)
            .addField("Reason", `Max Warnings`)
            .setColor('DARK_RED')
            .setFooter(`ID: (${mention.user.id})`, `https://png.icons8.com/ios/2x/hammer-filled.png`)
            .setTimestamp()
            bot.channels.get(modlog.id).send(embedlog);
        }

    else {
        warning = "Warning #1";

        mention.addRole(warning1).catch(console.log);

        warn();
        };

    function warn()
        {
        var embed = new Discord.RichEmbed()
        .setAuthor('Warning', 'http://www.clker.com/cliparts/H/Z/0/R/f/S/warning-icon-md.png')
        .setDescription("You have been warned.")
        .addField("Staff Member", `${message.member.user}`)
        .addField("Reason", `${reason}`)
        .setColor('ffa500')
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        mention.send(embed).catch(console.log);

        var embed = new Discord.RichEmbed()
        .setAuthor("User Warned", "http://www.clker.com/cliparts/H/Z/0/R/f/S/warning-icon-md.png")
        .setDescription(`${mention.user} has been warned.`)
        .addField("Staff Member", `${message.member.user}`)
        .addField("Reason", `${reason}`)
        .setColor('ffa500')
        .addBlankField()
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        message.channel.send(embed);

        if(!modlog) return;

        var embedlog = new Discord.RichEmbed()
        .setAuthor(`${mention.user.username}#${mention.user.discriminator}`, mention.user.avatarURL)
        .addField("User Warned", `${mention.user}`)
        .addField("Warning Added", `${warning}`)
        .addField("Staff Member", `${message.member.user}`)
        .addField("Reason", `${reason}`)
        .setColor('ffa500')
        .setFooter(`ID: (${mention.user.id})`, `http://www.clker.com/cliparts/H/Z/0/R/f/S/warning-icon-md.png`)
        .setTimestamp()
        bot.channels.get(modlog.id).send(embedlog);

        }

       
}

module.exports.help = {

    name: "warn"

}