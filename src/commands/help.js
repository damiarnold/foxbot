const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {
    
    message.reply("Direct messaged you the help.")
    var embed = new Discord.RichEmbed()
        .setAuthor("Help", `https://png.icons8.com/ios/540/about.png`)
        .setDescription("Commands listed below.")
        .addField("!info", "Shows the bot information.")
        .addField("!feedback", "Submit feedback.")
        .addField("!report", "Submit a bug report.")
        .addField("!ping", "Shows Foxbot's online status.")
        .addField("!userinfo", "Shows the mentioned users info.")
        .addField("!serverinfo", "Shows the server info.")
        //.addField("!invite", "Shows the server invites.")
        .addField("!avatar", "Shows the user's avatar.")
        //.addField("!help", "Shows this help box.")
        //.addField("!mod", "Shows mod commands.")
        .setFooter(`${bot.user.username} ${ver}`, bot.user.avatarURL)
        .setColor('DARK_RED')
    message.author.send(embed);
       
}

module.exports.help = {

    name: "help"

}