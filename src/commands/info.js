const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {
    
    var embed = new Discord.RichEmbed()
    .setAuthor("Info", "https://png.icons8.com/ios/540/about.png")
    .setDescription(`Created by [@itsfoxbit](https://twitter.com/itsfoxbit) - Foxbit#6845`)
    //.addField("Website", "[bot.foxbit.tech](http://bot.foxbit.tech)")
    .addField("Version", `${ver}`)
    .addField("Server Count", `${bot.guilds.size}`)
    .addField("Uptime", `${bot.uptime}`)
    .addField(`Support Server`, `[discord.gg/vQKHvHm](https://discord.gg/vQKHvHm)`)
    .addField("Created On", `${bot.user.createdAt}`)
    .setColor('DARK_RED')
    .setFooter(`${bot.user.username} ${ver}`,bot.user.avatarURL)
    .setThumbnail(bot.user.avatarURL)
    message.channel.send(embed);
       
}

module.exports.help = {

    name: "info"

}