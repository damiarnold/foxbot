const Discord = require("discord.js");
const botSettings = require("../botsettings.json");

const ver = botSettings.ver;

module.exports.run = async (bot, message, args) => {

    const checkpoint = message.member.guild.roles.find('name', 'Checkpoint');
    const modlog = message.guild.channels.find('name', 'mod-log');
    let member = message.member;
    
    if(checkpoint)
    {
        let role = message.guild.roles.find("name", "Checkpoint");

        message.member.removeRole(role).catch(console.error);
    
        var embedlog = new Discord.RichEmbed()
        .setAuthor(`${member.user.username}#${member.user.discriminator}`, member.user.avatarURL)
        .addField("Checkpoint Passed", `${member.user}`)
        .setColor('BLUE')
        .setFooter(`ID: (${member.user.id})`, `https://png.icons8.com/ios/540/about.png`)
        .setTimestamp()
        bot.channels.get(modlog.id).send(embedlog)
        
    }

    message.delete();
       
}

module.exports.help = {

    name: "agree"

}